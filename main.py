import irc.bot
import irc.strings
from datetime import datetime, time, timedelta
import random

timeBetweenCommits = timedelta(seconds=600)

class CUSFSBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel
        self.storyFile = open("chain" + datetime.now().strftime("%y-%m-%d-%H:%M:%S") + ".txt", "w")
        self.story = ""

        self.suggestions = []
        self.timesincelastcommit = datetime.now()

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)

    def on_pubmsg(self, c, e):
        message = e.arguments[0]
        if message[0] == "!":
            self.command(message[1:], e.source.nick)

    def command(self, message, nick):
        c = message.split(" ", 1)
        cmd = c[0]

        if cmd == "suggest":
            if len(c[1]) <= 210:
                self.suggestions.append({"sentence": c[1], "proposer": nick, "votes": {nick: 1}})
                self.connection.notice(self.channel, "New suggestion from %s" % nick)
                self.connection.notice(self.channel, c[1])
                self.connection.notice(self.channel, "Type !vote %i to vote for it" % (len(self.suggestions)))
            else:
                self.connection.privmsg(nick, "Suggestion is too long (max 210 characters)")
        elif cmd == "show":
            self.connection.notice(self.channel, "Current suggestions:")
            for i in range(len(self.suggestions)):
                s = self.suggestions[i]
                self.connection.notice(self.channel, "\"%s\" suggested by %s (%i votes, !vote %i to vote)" % (s["sentence"], s["proposer"], len(s["votes"]), i+1))
        elif cmd == "vote":
            try:
                number = int(c[1]) - 1
                if 0 <= number and number < len(self.suggestions):
                    if self.suggestions[number]["votes"].has_key(nick):
                        self.connection.notice(nick, "You've already voted for that one!")
                    else:
                        self.suggestions[number]["votes"][nick] = 1
                else:
                    self.connection.notice(nick, "Bad voting ID")
            except ValueError:
                self.connection.notice(nick, "Bad voting ID")
        elif cmd == "commit":
            if datetime.now() - self.timesincelastcommit > timeBetweenCommits:
                total_votes = 0
                for i in range(len(self.suggestions)):
                    total_votes += len(self.suggestions[i]["votes"])

                if total_votes == 0:
                    self.connection.notice(self.channel, "There aren't any suggestions...")
                else:
                    selection = random.randint(0, total_votes-1)

                    for i in range(len(self.suggestions)):
                        selection -= len(self.suggestions[i]["votes"])
                        if(selection <= 0):
                            self.story = self.story + "\n" + self.suggestions[i]["sentence"]
                            self.storyFile.write("\n" + self.suggestions[i]["sentence"])
                            self.connection.notice(self.channel, "Used the suggestion by %s" % self.suggestions[i]["proposer"])
                            self.connection.notice(self.channel, self.suggestions[i]["sentence"])
                            self.connection.notice(self.channel, "The story so far:")
                            for line in self.story.split("\n"):
                                self.connection.notice(self.channel, line)

                            self.suggestions = []
                            self.timesincelastcommit = datetime.now()
                            return
            else:
                self.connection.notice(nick, "It hasn't been long enough since the last commit, %s left" % str(self.timesincelastcommit - datetime.now() + timeBetweenCommits))
        elif cmd == "storysofar":
            for line in self.story.split("\n"):
                self.connection.notice(nick, line)

def main():
    import sys
    random.seed()
    if len(sys.argv) != 4:
        print("Usage: bot <server[:port]> <channel> <nickname>")
        sys.exit(1)

    s = sys.argv[1].split(":", 1)
    server = s[0]

    if len(s) == 2:
        try:
            port = int(s[1])
        except ValueError:
            print("Bad port number")
            sys.exit(1)
    else:
        port = 6667

    channel = sys.argv[2]
    nickname = sys.argv[3]

    bot = CUSFSBot(channel, nickname, server, port)
    bot.start()

if __name__ == "__main__":
    main()
